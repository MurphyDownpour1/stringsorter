<?php

namespace src;

class StringSorter extends CharsSorter
{
    private $originalText = "";
    private $sortedText = "";

    public function __construct(string $text)
    {
        $this->originalText = $text;
    }

    /**
     *
     * Сортирует символы слов в тексте в алфавитном порядке
     *
     * @return string
     */
    public function sortAlphabetic() : string
    {
        $words_arr = explode(" ", $this->originalText);

        foreach($words_arr as $word)
        {
            $word = mb_strtolower($word, 'UTF-8');
            $temp_arr =  preg_split('##u', $word, -1, PREG_SPLIT_NO_EMPTY);
            sort($temp_arr);
            $this->sortedText .= implode($temp_arr) . " ";
        }
        $result = $this->charsCaseComparator($this->originalText, $this->sortedText);

        return trim($result);
    }
}