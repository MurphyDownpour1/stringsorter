<?php

namespace src;

class CharsSorter
{
    private $upperCaseChars = [];

    /**
     * Сравнивает исходный текст с отсортированным.
     * Ищет в исходном тексте буквы верхнего регистра, фиксирует их, а затем
     * в отсортированном тексте переводит эти же символы в верхний регистр
     *
     * @param string $originalText Исходный текст
     * @param string $sortedText Отсортированный текст
     * @return string Конечный результат
     */
    public function charsCaseComparator(string $originalText, string $sortedText) : string
    {
        $originalWords = explode(' ', $originalText);
        $sortedWords = explode(' ', $sortedText);

        $this->searchUpperCase($originalWords);

        $result = $this->transformToOriginalCase($sortedWords);
        $result = implode(' ', $result);

        return $result;
    }

    /**
     *
     * Ищет в исходном тексте буквы верхнего регистра и фиксирует их.
     *
     * @param array $words массив со словами из текста
     */
    private function searchUpperCase(array $words) : void
    {
        for ($i = 0; $i < count($words); $i++)
        {
            for ($j = 0; $j < strlen($words[$i]); $j++)
            {
                $character = $words[$i][$j];

                if ($character != strtolower($words[$i][$j]))
                {
                    $this->upperCaseChars[$i][] = strtolower($words[$i][$j]);
                }
            }
        }
    }

    /**
     * Принимает массив отсортированных слов и заменяет некоторые символы на
     * те же символы верхнего регистра, сравнивая с оригиналом.
     *
     * @param array $words массив отсортированных слов
     * @return array массив с результирующими словами
     */
    private function transformToOriginalCase(array $words) : array
    {
        for ($i = 0; $i < count($words); $i++)
        {
            /* Ищем в списке заглавных символов и сравниваем с текущим
            символов. Если совпадает - переводим его в верхний регистр */
            $iteration = count((array) $this->upperCaseChars[$i]);
            for ($j = 0; $j < $iteration; $j++)
            {
                $words = (array) $words;
                $position = strpos($words[$i], $this->upperCaseChars[$i][$j]);
                if ($position >= 0)
                    $words[$i][$position] = ucfirst($this->upperCaseChars[$i][$j]);

            }
        }

        return $words;
    }
}