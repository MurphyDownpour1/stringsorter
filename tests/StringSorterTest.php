﻿<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use src\StringSorter;

class StringSorterTest extends TestCase
{
    /**
     * @dataProvider providerSortAlphabetic
     */
    public function testSortAlphabetic($a, $b)
    {
        $obj = new StringSorter($a);
        $this->assertEquals($b, $obj->sortAlphabetic());
    }

    public function providerSortAlphabetic ()
    {
        return [
            ['bare', 'aber'],
            ['granny', 'agnnry'],
            ['rabbit', 'abbirt'],
            ['roBot', 'Boort'],
            ['smIle', 'eIlms'],
            ['123', '123'],
            ['312', '123'],
            ['true', 'ertu'],
            ['null', 'llnu']
        ];
    }
}