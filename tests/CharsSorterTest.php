<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use src\CharsSorter;

class CharsSorterTest extends TestCase
{
    /**
     * @dataProvider providerCharsCaseComparator
     */
    public function testCharsCaseComparator($a, $b, $c)
    {
        $obj = new CharsSorter();
        $this->assertEquals($c, $obj->charsCaseComparator($a, $b));
    }

    public function providerCharsCaseComparator ()
    {
        return [
            ['αβγαβγ αβγαβγαβγ', 'ααββγγ αααβββγγγ', 'ααββγγ αααβββγγγ'],
            ['lemon orange banana apple', 'elmno aegnor aaabnn aelpp', 'elmno aegnor aaabnn aelpp'],
            ['deAth strAnding', 'adeht adginnrst', 'Adeht Adginnrst'],
            ['лимон апельсин банан яблоко', 'илмно аеилнпсь аабнн бклооя', 'илмно аеилнпсь аабнн бклооя']
        ];
    }
}